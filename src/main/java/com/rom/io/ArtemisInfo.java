package com.rom.io;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.activemq.artemis.nativo.jlibaio.SubmitInfo;

public class ArtemisInfo implements SubmitInfo {

    static AtomicInteger count = new AtomicInteger();

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        count.decrementAndGet();
    }

    boolean error = false;
    String errorMessage;
    int errno;
    boolean done = false;

    ArtemisInfo() {
        count.incrementAndGet();
    }

    public void reset() {
        error = false;
        errorMessage = null;
        errno = 0;
        done = false;
    }

    public void validate() {
        if (error || errno != 0) {
            throw new IllegalStateException("Error: " + errno);
        } /*else if (!done) {
            throw new IllegalStateException("Not done");
        }*/
    }

    @Override
    public void onError(int errno, String message) {
        this.errno = errno;
        this.errorMessage = message;
        this.error = true;
    }

    @Override
    public void done() {
        done = true;
    }

    public boolean isDone() {
        return done;
    }

    public int getErrno() {
        return errno;
    }

    public void setErrno(int errno) {
        this.errno = errno;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "ArtemisInfo [done=" + done + ", errno=" + errno + ", error=" + error + ", errorMessage=" + errorMessage
                + "]";
    }

}