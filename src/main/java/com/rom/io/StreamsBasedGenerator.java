package com.rom.io;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Roman Katerinenko
 */
final class StreamsBasedGenerator extends BufferedGenerator {

    private BufferedOutputStream outputStream;

    StreamsBasedGenerator(TestParams testParams) {
        super(testParams);
    }

    @Override
    protected void openFileAndBuffers() throws FileNotFoundException {
        outputStream = new BufferedOutputStream(new FileOutputStream(getTestParams().getFilePath()));
    }

    @Override
    protected long generateNextLine() throws IOException {
        long req = Math.min(getRemainderBytes(), getTestParams().getBufferSizeInBytes());
        long rounds = req / 4096;
        for (long r = 0; r < rounds; r++) {
            outputStream.write(FONS);
        }
        long rem = req - (rounds * 4096);
        if (rem > 0) {
            System.err.println("Remaining: " + rem);
            outputStream.write(FONS, 0, (int) rem);
        }
        return req;
    }

    @Override
    public void close() throws IOException {
        if (outputStream != null) {
            outputStream.close();
        }
    }

    @Override
    public String getName() {
        return "StreamsBasedGenerator";
    }
}