package com.rom.io;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.apache.activemq.artemis.nativo.jlibaio.LibaioContext;
import org.apache.activemq.artemis.nativo.jlibaio.LibaioFile;
import org.json.simple.JSONObject;

/**
 * @author Damon Brown
 */
final class ArtemisInput extends Testable {

    private static final int LIBAIO_QUEUE_SIZE = 50;

    private int queueSize = LIBAIO_QUEUE_SIZE;

    private final TestParams testParams;

    public LibaioContext<ArtemisInfo> control;
    private LibaioFile<ArtemisInfo> fileDescriptor;

    boolean direct = false;
    boolean semaphore = true;
    boolean fdataSync = false;
    boolean validate = false;

    ByteBuffer[] buffers;
    ArtemisInfo[] callbacks;

    ArtemisInput(TestParams testParams) {
        this.testParams = testParams;
        queueSize = Integer.parseInt(System.getProperty("libaio.queue.size", "50"));
        validate = Boolean.getBoolean("artemis.validate");
    }

    @Override
    public JSONObject recordConfig() {
        JSONObject cfg = new JSONObject();
        cfg.put("direct", direct);
        cfg.put("semaphore", semaphore);
        cfg.put("fdataSync", fdataSync);
        cfg.put("queueSize", queueSize);
        return cfg;
    }

    @Override
    public void init() throws IOException {
        int bsize = LibaioContext.getBlockSize(this.testParams.getFilePath());

        control = new LibaioContext<>(queueSize, semaphore, fdataSync);
        fileDescriptor = control.openFile(testParams.getFilePath(), direct);

        callbacks = new ArtemisInfo[queueSize];
        buffers = new ByteBuffer[queueSize];
        for (int i = 0; i < queueSize; i++) {
            callbacks[i] = new ArtemisInfo();
            buffers[i] = ByteBuffer.allocateDirect(this.testParams.getBufferSizeInBytes());//LibaioContext.newAlignedBuffer(this.testParams.getBufferSizeInBytes(), bsize);
        }
    }

    @Override
    public void run() throws IOException {
        long fremaining = this.testParams.getFileSizeInBytes();
        int bsize = this.testParams.getBufferSizeInBytes();
        int base = 0;
        long findex = 0;
        while (fremaining > 0) {
            if (debug) {
                debug("Rem: " + fremaining);
            }
            int idx = 0;
            for (/* idx */; fremaining > 0 && idx < queueSize; idx++) {
                if (debug) {
                    debug("idx: " + idx);
                }
                int limit = (int) Math.min(bsize, fremaining);
                ByteBuffer buffer = buffers[idx];
                buffer.clear();
                callbacks[idx] = new ArtemisInfo();
                if (debug) {
                    debug("read @ " + base + ", " + limit + " b");
                }
                fileDescriptor.read(base, limit, buffer, callbacks[idx]);
                fremaining -= limit;
                base += limit;
                if (debug) {
                    debug("after read " + fremaining);
                }
            }
            if (debug) {
                debug("after idx: " + idx);
            }

            if (debug) {
                debug("Round " + idx);
            }
            int polling = control.poll(callbacks, idx, idx);
            if (debug) {
                debug("Polled: " + polling);
            }
            if (polling < idx) {
                throw new IllegalStateException("Min not met: " + polling + " < " + idx);
            }
            if (!validate) {
                continue;
            }
            for (int i = 0; i < idx; i++) {
                callbacks[i].validate();
                buffers[i].rewind();

                for (int bi = 0; bi < bsize && findex < testParams.getFileSizeInBytes(); bi++) {
                    int fval = (int) (findex % 256);
                    byte bval = (byte) (0xFF & buffers[i].get());
                    if (bval != VALUES[fval]) {
                        System.err.println("Sample of: " + buffers[i]);
                        String sep = "";
                        for (int j = 0; j < buffers[i].capacity(); j++) {
                            System.err.print(sep + Integer.toHexString(0xFF & buffers[i].get(j)));
                            sep = " ";
                        }
                        System.err.println();
                        System.err.flush();
                        throw new IllegalStateException("Expected 0x" + Integer.toHexString(0xFF & VALUES[fval])
                                + ", got: 0x" + Integer.toHexString(bval) + " at " + findex + " i:" + i + ", bi:" + bi);
                    }
                    findex++;
                }
                if (debug) {
                    debug("Validated polled index: " + i);
                }
            }
        }
    }

    @Override
    public void close() throws IOException {
        if (direct) {
            for (ByteBuffer bb : buffers) {
                LibaioContext.freeBuffer(bb);
            }
        }
        fileDescriptor.close();
        control.close();
    }

    @Override
    public String getName() {
        return "ArtemisInput";
    }
}