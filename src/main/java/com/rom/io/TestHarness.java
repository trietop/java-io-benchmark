package com.rom.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import static java.lang.String.format;
/**
 * @author Roman Katerinenko
 */
@SuppressWarnings("FieldCanBeLocal")
public final class TestHarness {
    private final Collection<Testable> testables = new ArrayList<>();
    private final long[] timesNano;
    private final TestParams testParams;

    private long minNanos;
    private long maxNanos;
    private long averageNanos;
    private double sygma;
    private Testable testable;
    private double sygmaPercent;
    private boolean add;

    private String fname = System.getProperty("file.prefix", "test_run");
    private String testFilter = System.getProperty("test.filter");

    public static void main(String... args) throws IOException {
        TestParams testParams = new TestParams().setBufferSizeInBytes(1024).setFilePath("1m.txt")
                .setFileSizeInBytes(1024 * 1024 * 1024).setRepeats(2);
        runAllTestsWith(testParams);
    }

    public static void runAllTestsWith(TestParams testParams) throws IOException {
        System.err.printf("%s\n-------------------\n", testParams);
        TestHarness testHarness = new TestHarness(testParams);

        testHarness.addTestable(new DirectByteBufferGenerator(testParams));
        testHarness.addTestable(new DirectByteBufferInput(testParams));
        testHarness.addTestable(new StreamsBasedGenerator(testParams));
        testHarness.addTestable(new StreamsBasedInput(testParams));
        testHarness.addTestable(new IndirectByteBufferGenerator(testParams));
        testHarness.addTestable(new IndirectByteBufferInput(testParams));
        testHarness.addTestable(new MemoryMappedGenerator(testParams));
        testHarness.addTestable(new MemoryMappedInput(testParams));

        testHarness.addTestable(new ArtemisGenerator(testParams));
        testHarness.addTestable(new ArtemisInput(testParams));
        testHarness.run();
    }

    TestHarness(TestParams testParams) {
        this.testParams = testParams;
        this.timesNano = new long[testParams.getRepeats()];
    }

    @SuppressWarnings("WeakerAccess")
    void addTestable(Testable testable) {
        testables.add(testable);
    }

    @SuppressWarnings("WeakerAccess")
    void run() throws IOException {
        JSONObject testRun = new JSONObject();

        JSONObject params = new JSONObject();
        params.put("timestamp", System.currentTimeMillis());
        params.put("repeats", testParams.getRepeats());
        params.put("bufferSize", testParams.getBufferSizeInBytes());
        params.put("path", testParams.getFilePath());
        params.put("fileSize", testParams.getFileSizeInBytes());
        params.put("warmup", testParams.getWarmup());
        testRun.put("params", params);

        JSONObject java = new JSONObject();
        java.put("vm", System.getProperty("java.vm.name"));
        java.put("version", System.getProperty("java.version"));
        java.put("runtime_version", System.getProperty("java.runtime.version"));
        java.put("vm_version", System.getProperty("java.vm.version"));
        java.put("class_version", System.getProperty("java.class.version"));
        java.put("os_arch", System.getProperty("os.arch"));
        testRun.put("java", java);

        testRun.put("memory", getMemory());

        JSONArray data = new JSONArray();
        for (Testable testable : testables) {
            System.gc();
            this.testable = testable;
            if (testFilter != null && !this.testable.getName().contains(testFilter)) {
                continue;
            }
            System.err.println("Testing: " + testable.getName());

            for (int i = 0, n = testParams.getRepeats() + testParams.getWarmup(); i < n; i++) {
                testable.init();
                long op = System.nanoTime();
                testable.run();
                testable.close();
                long st = System.nanoTime();
                if (i - testParams.getWarmup() >= 0) {
                    timesNano[i - testParams.getWarmup()] = st - op;
                }
            }
            computeStats();
            printStats();
            JSONObject res = toJson(testable);
            res.put("memory", getMemory());
            data.add(res);
            System.err.println(res.toJSONString());
        }
        testRun.put("data", data);
        // Write JSON file
        new File("data").mkdirs();
        try (FileWriter file = new FileWriter(String.format("data/%s_%s_%d.json", fname,
                System.getProperty("java.version"), System.currentTimeMillis()))) {
            file.write(testRun.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(testRun.toJSONString());
    }

    private void computeStats() {
        minNanos = Arrays.stream(timesNano).min().orElseThrow(() -> new IllegalStateException("No min"));
        maxNanos = Arrays.stream(timesNano).max().orElseThrow(() -> new IllegalStateException("No max"));
        averageNanos = (long) Arrays.stream(timesNano).average()
                .orElseThrow(() -> new IllegalStateException("No average"));
        sygma = computeSygma();
        long delta = maxNanos - minNanos;
        if (delta > 0) {
            sygmaPercent = (sygma / (double) delta) * 100.;
        } else {
            sygmaPercent = 0;
        }
    }

    JSONObject toJson(Testable test) {
        JSONObject data = new JSONObject();
        data.put("test", test.getName());
        data.put("avg", averageNanos);
        data.put("min", minNanos);
        data.put("max", maxNanos);
        data.put("sygma", sygma);
        data.put("sygmaPct", sygmaPercent);
        JSONArray times = new JSONArray();
        Arrays.stream(timesNano).forEach(t -> times.add(t));
        data.put("timing", times);
        JSONObject cfg = testable.recordConfig();
        if (cfg != null) {
            data.put("config", cfg);
        }
        return data;
    }

    private void printStats() {
        System.err.printf("%s:\navg:%s\nmin:%s\nmax:%s\nsdev:%s (%f%%)\n", testable.getName(),
                formatNanos(averageNanos), formatNanos(minNanos), formatNanos(maxNanos), formatSygma(sygma),
                sygmaPercent);
        System.err.print("runs (millis):");
        Arrays.stream(timesNano).forEach(t -> System.err.format("%s, ", t / 1000000));
        System.err.println("\n");
        System.err.println("--------------------------");
    }

    JSONObject getMemory() {
        JSONObject mem = new JSONObject();
        Runtime run = Runtime.getRuntime();
        mem.put("proc", run.availableProcessors());
        mem.put("free", run.freeMemory());
        mem.put("max", run.maxMemory());
        mem.put("total", run.totalMemory());
        return mem;
    }

    private long computeSygma() {
        long sum = 0;
        for (long time : timesNano) {
            sum += (averageNanos - time) * (averageNanos - time);
        }
        return (long) (Math.sqrt(sum / timesNano.length));
    }

    private String formatSygma(double nanos) {
        int mio = 1000000;
        if (nanos < mio) {
            return format("%f micros", nanos / 1000.);
        } else {
            return format("%f millis", nanos / (double) mio);
        }
    }

    private String formatNanos(double nanos) {
        int mio = 1000000;
        if (nanos < mio) {
            return format("%f micros %s", nanos / 1000., getMbSfor(nanos));
        } else {
            return format("%f millis %s", nanos / (double) mio, getMbSfor(nanos));
        }
    }

    private String getMbSfor(double nanos) {
        double perSec = 1_000_000_000. / nanos;
        double megabytes = testParams.getFileSizeInBytes() / 1024. / 1024.;
        double MbS = megabytes * perSec; // megabytes/sec
        return format("%.3f (MB/s)", MbS);
    }
}