package com.rom.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

import org.json.simple.JSONObject;

/**
 * @author Roman Katerinenko
 */

abstract class Testable {
    static final byte VALUE = (byte) 123;

    static final byte[] VALUES = new byte[256];

    static final byte[] FONS = new byte[4096];

    static {
        for (int i = 0; i < 256; i++) {
            VALUES[i] = (byte) (0xFF & i);
        }
        ByteBuffer bb = ByteBuffer.wrap(FONS);
        for (int i = 0; i < 16; i++) {
            bb.put(VALUES);
        }
    }

    boolean debug = Boolean.getBoolean("io.debug");

    abstract void init() throws IOException;

    abstract void run() throws IOException;

    abstract void close() throws IOException;

    public String getName() {
        return getClass().getName();
    }

    public JSONObject recordConfig() {
        return null;
    }

    void debug(String message) {
        if (debug) {
            System.err.println(message);
        }
    }
}