package com.rom.io;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.apache.activemq.artemis.nativo.jlibaio.LibaioContext;
import org.apache.activemq.artemis.nativo.jlibaio.LibaioFile;
import org.json.simple.JSONObject;

/**
 * @author Damon Brown
 */
public class ArtemisGenerator extends BufferedGenerator {
    private static final int LIBAIO_QUEUE_SIZE = 50;

    private int queueSize = LIBAIO_QUEUE_SIZE;

    public LibaioContext<ArtemisInfo> control;

    private LibaioFile<ArtemisInfo> fileDescriptor;
    private ByteBuffer[] buffers;

    boolean direct = true;
    boolean semaphore = true;
    boolean fdataSync = true;

    ArtemisInfo[] callbacks;

    ArtemisGenerator(TestParams testParams) {
        super(testParams);
        queueSize = Integer.parseInt(System.getProperty("libaio.queue.size", "50"));
    }

    @Override
    public void close() throws IOException {
        for (ByteBuffer bb : buffers) {
            LibaioContext.freeBuffer(bb);
        }
        fileDescriptor.close();
        control.close();
    }

    @Override
    public String getName() {
        return "ArtemisGenerator";
    }

    @Override
    public JSONObject recordConfig() {
        JSONObject cfg = new JSONObject();
        cfg.put("direct", direct);
        cfg.put("semaphore", semaphore);
        cfg.put("fdataSync", fdataSync);
        cfg.put("queueSize", queueSize);
        return cfg;
    }

    @Override
    protected void openFileAndBuffers() throws IOException {
        int bsize = LibaioContext.getBlockSize(getTestParams().getFilePath());
        // System.err.println("Block size: " + bsize);
        control = new LibaioContext<>(queueSize, semaphore, fdataSync);
        // System.err.println("Control: " + control);
        fileDescriptor = control.openFile(getTestParams().getFilePath(), direct);
        // fileDescriptor.fallocate(this.getTestParams().getFileSizeInBytes());
        // System.err.println("fd: " + fileDescriptor);
        buffers = new ByteBuffer[queueSize];
        callbacks = new ArtemisInfo[queueSize];
        for (int i = 0; i < buffers.length; i++) {
            buffers[i] = LibaioContext.newAlignedBuffer(getTestParams().getBufferSizeInBytes(), bsize);
            callbacks[i] = new ArtemisInfo();
        }
    }

    @Override
    protected long generateNextLine() throws IOException {
        // long length = getCrBytes().length;
        // long bytesToGenerate = Math.min(getRemainderBytes(),
        // getTestParams().getBufferSizeInBytes() - length);
        long bytesToGenerate = getRemainderBytes();
        long generated = 0L;

        int idx = 0;
        long findex = 0;
        int window = getTestParams().getBufferSizeInBytes();
        while (bytesToGenerate > 0 && idx < queueSize) {
            callbacks[idx] = new ArtemisInfo();
            ByteBuffer buffer = buffers[idx];
            buffer.clear();

            long req = Math.min(window, bytesToGenerate);
            long pos = findex;
            /*
            // System.err.println("Writing " + req + " bytes");
            for (int i = 0; i < req; i++) {
                int fval = (int) (findex % 256);
                buffer.put(VALUES[fval]);
                findex++;
            }*/
            long rounds = req / 4096;
            for (long r = 0; r < rounds; r++) {
                buffer.put(FONS);
                findex += 4096;
            }
            long rem = req - (rounds * 4096);
            if (rem > 0) {
                System.err.println("Remaining: " + rem);
                buffer.put(FONS, 0, (int) rem);
                findex += rem;
            }
            generated += req;
            bytesToGenerate -= req;

            buffer.rewind();
            // System.err.println(idx + " @ " + idx * window + ", b: " + req + ", buf: " +
            // buffer + ", cb: " + callback);
            fileDescriptor.write(pos, (int) req, buffer, callbacks[idx]);
            idx += 1;
        }

        int polling = control.poll(callbacks, idx, idx);
        if (polling < idx) {
            throw new IllegalStateException("Min not met: " + polling + " < " + idx);
        }
        // System.err.println("Polling for " + idx + ", got: " + polling + ", bytes: " +
        // generated);
        for (int i = 0; i < idx; i++) {
            try {
                callbacks[i].validate();
            } catch (IllegalStateException ise) {
                System.err.println(callbacks[i]);
            }
        }
        return generated;
    }
}