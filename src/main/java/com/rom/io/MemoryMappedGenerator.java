package com.rom.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;

import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;

/**
 * @author Roman Katerinenko
 */
final class MemoryMappedGenerator extends BufferedGenerator {
    private MappedByteBuffer fileBuffer;
    private FileChannel fileChannel;

    MemoryMappedGenerator(TestParams testParams) {
        super(testParams);
    }

    @Override
    public void close() throws IOException {
        if (fileBuffer != null) {
            fileBuffer.force();
        }
        if (fileChannel != null){
            fileChannel.close();
        }
    }

    @Override
    public String getName() {
        return "MemoryMappedGenerator";
    }

    @Override
    protected void openFileAndBuffers() throws IOException {
        fileChannel = FileChannel.open(Paths.get(getTestParams().getFilePath()), READ, WRITE);
        fileBuffer = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0, getTestParams().getFileSizeInBytes());
    }

    @Override
    protected long generateNextLine() throws IOException {
        fileBuffer.clear();
        /*
        long length = getCrBytes().length;
        long bytesToGenerate = Math.min(getRemainderBytes(), getTestParams().getBufferSizeInBytes() - length);
        for (int i = 0; i < bytesToGenerate; i++) {
            fileBuffer.put(VALUE);
        }
        fileBuffer.put(getCrBytes());
        return bytesToGenerate + length;
        */
        long req = Math.min(getRemainderBytes(), getTestParams().getBufferSizeInBytes());
        long rounds = req / 4096;
        for (long r = 0; r < rounds; r++) {
            fileBuffer.put(FONS);
            //findex += 4096;
        }
        long rem = req - (rounds * 4096);
        if (rem > 0) {
            System.err.println("Remaining: " + rem);
            fileBuffer.put(FONS, 0, (int) rem);
            //findex += rem;
        }
        return req;
    }
}